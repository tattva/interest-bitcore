var inherits = require('util').inherits;
var bitcore = require('bitcore-lib');
var Transaction = bitcore.Transaction;
var EventEmitter = require('events').EventEmitter;
var WalletClient = require('bitcore-wallet-client');
var BWS_INSTANCE_URL = 'http://localhost:3232/bws/api';

function InterestBitcore(options) {
  EventEmitter.call(this);
  this.node = options.node;
  this.bus = this.node.openBus();  
  this.bus.on('address/transaction', function(transaction) {
    // TODO: notify rails server of transaction
    console.log(transaction);
  });
}

inherits(InterestBitcore, EventEmitter);

InterestBitcore.dependencies = ['bitcoind', 'db', 'address'];

InterestBitcore.prototype.start = function(callback) {
  setImmediate(callback);
};

InterestBitcore.prototype.stop = function(callback) {
  setImmediate(callback);
};

InterestBitcore.prototype.subscribeTransactions = function(address) {

}

InterestBitcore.prototype.getAPIMethods = function() {
  return [];
};

InterestBitcore.prototype.getPublishEvents = function() {
  return [];
};

module.exports = InterestBitcore;

InterestBitcore.prototype.setupRoutes = function(app, express) {

  interestBitcore = this;

  // create a new wallet and subscribe to transaction events.
  app.get('/create_wallet', function(req, res) {
    
    var client = new WalletClient({
      baseUrl: BWS_INSTANCE_URL,
      verbose: false,
    });

    client.createWallet("My Wallet", "username", 1, 1, {network: 'testnet'}, function(err, secret) {

      // save wallet details to dat file.
      var fs = require('fs');
      fs.writeFileSync('wallet.dat', client.export());

      // create address for this wallet.
      client.createAddress(null, function(err, obj) {

        res.send("Wallet created with address: " + obj.address);

        // subscribe to wallet transactions
        interestBitcore.bus.subscribe('address/transaction', [obj.address]);
      });

    });

  });

  // checks balance of the current wallet
  app.get('/check_existing_wallet', function(req, res) {

    var client = new WalletClient({
      baseUrl: BWS_INSTANCE_URL,
      verbose: false
    });

    var fs = require('fs');
    fs.readFile('wallet.dat', function read(err, data) {
      if (err) {
        res.send("No existing wallet.");
      }
      client.import(data);
      console.log("imported data - " + data);

      client.getBalance(null, function(err, obj) {
        res.send(JSON.stringify(obj));
      });
    });
    
  });

  // Serve static content
  app.use('/static', express.static(__dirname + '/static'));
};

InterestBitcore.prototype.getRoutePrefix = function() {
  return 'interest-bitcore'
};